.PHONY: test run clean build dist

VERSION ?=1.0.0

test:
	@npm test

clean:
	@rm -rf node_modules

build:
	@npm install

dist:
	@docker build -t simple-nodejs:${VERSION} -t simple-nodejs:latest -f Dockerfile .
	# TODO: Distribute to docker registry

